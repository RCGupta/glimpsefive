<?php 
    $page = 'index';
    include "header.php";?>





<section class="banner">
    <div class="container-fluid">
        <div class="row hidden-xs">
            <div class="col-xs-12 col-sm-7">
                <!-- <img class="img-responsive" src="images/banner.png"> -->
                <h1 class="main-banner-content"><span class="banner-word-2"> People Management Solutions </span> <br> for mapping the talent and incredible onboard experience.</h1>
            </div>
            <div class="col-xs-12 col-sm-5 banner-img">
                <img class="img-responsive" src="images/banner-image.png">
            </div>
        </div>
        
        <div class="row hidden-sm visible-xs">
            <div class="col-xs-12 col-sm-5 banner-img">
                <img class="img-responsive" src="images/banner-image.png">
            </div>
            <div class="col-xs-12 col-sm-7">
                <!-- <img class="img-responsive" src="images/banner.png"> -->
                <h1 class="main-banner-content"><span class="banner-word-2"> People Management Solution </span> that helps in mapping talent with excellent onboard experiences for your organisation.</h1>
            </div>
        </div>
    </div>
</section>



<div id="colorlib-page">
    <div class="js-fullheight banner-section">
        <!--  id="colorlib-hero" -->
        <div class="container-fluid">
            <div class="row">
                <h2 class="text-center text-uppercase">Offerings</h2>
                <div class="col-xs-12 col-md-6">
                    <div class="banner-section-one">
                        <div class="align-middle">
                            <div class="col-xs-8 wow fadeInUp animated">
                                <h1>Talent Hub</h1>
                                <ul class="talent-search-points">
                                    <li> Private Talent Pool of regular skills </li>
                                    <li> Personalised Branding about your company </li>
                                    <li> Job Campaigns with > 50K validated talent </li>
                                </ul>
                                <a href="services.php">Read More</a>
                                <a href="job-seekers.php">Upload CV</a>
                            </div>
                            <div class="col-xs-4 col-lg-pull-1 wow fadeInDown animated">
                                <img class="img-responsive" src="images/banner-img-01.png" width="200">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="banner-section-two">
                        <div class="align-middle">
                            <div class="col-xs-8 wow fadeInUp animated">
                                <h1>Talent Onboard Software</h1><!-- Onboarding -->
                                <ul class="talent-search-points">
                                    <li> Create vibrant First Day Onboard Experience </li>
                                    <li> Digitise Joining Formalities & Processes</li>
                                    <li> Happy Employees become your Brand Ambassadors</li>
                                </ul>
                                <a href="products.php">Read More</a>
                                <a href="demo.php">Demo</a>
                            </div>
                            <div class="col-xs-4 col-lg-pull-1 wow fadeInDown animated">
                                <img class="img-responsive" src="images/banner-img-02.png" width="200">
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="align-middle-demo hidden">
                    <h1 class="wow fadeInDown animated">Start your free Demo</h1>
                    <!-- <a href="#">Know More</a> -->
                    <!--  -->
                    <div class="col-md-6 col-md-offset-3">
                        <!--
                                <form action="#">
                                    <div class="input-group wow fadeInUp animated">
                                        <input class="btn btn-lg" name="email" id="email" type="email" placeholder="Your Email" required>
                                        <button class="btn btn-info btn-lg" type="submit">Let's Start</button>
                                    </div>
                                </form>
                            -->
                        <div class="banner-form">
                            <!--[if lte IE 8]>
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                <![endif]-->
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                            <script>
                                hbspt.forms.create({
                                    portalId: "3004220",
                                    formId: "9c61fecd-d840-436f-a74b-e7b79dc02f4d"
                                });

                            </script>
                        </div>
                    </div>
                    <!-- /. -->
                </div>
                <!-- /. -->
            </div>
        </div>
    </div>
</div>

<!--
<section class="clients">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="client-logos">
                    <h2 class="wow animated flipInX">They Trusted Us</h2>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInUp animated wow"><img class="img-responsive" src="images/capgemini.png"></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/kpit.png"></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInUp animated wow"><img class="img-responsive" src="images/jci.png"></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/icertis.png"></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInUp animated wow"><img class="img-responsive" src="images/itc.png"></div>
                    <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/larsen.png"></div>

                    <div class="clients-logos-bottom">
                        <div class="col-xs-6 col-sm-4 col-md-3 hidden-xs"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/inspirage.png"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/cerillion.png"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2 fadeInDown animated wow"><img class="img-responsive" src="images/drivedge.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
-->

<section id="demos" class="clients">
    <div class="container">
        <div class="row">
            <h2 class="wow animated flipInX text-center">OUR CLIENTELE</h2>
            <div class="large-12 columns">
                <div class="owl-carousel-logos owl-theme">
                    <div class="item">
                        <img class="img-responsive" src="images/capgemini.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/kpit.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/jci.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/icertis.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/itc.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/larsen.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/inspirage.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/cerillion.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/drivedge.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/globant.png">
                    </div>
                    <div class="item">
                        <img class="img-responsive" src="images/iimu.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<!-- <section class="who-we-are">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center fadeInDown animated wow">
                    <div class="col-sm-2 col-xs-hidden"></div>
                    <div class="col-sm-8 col-xs-hidden">
                        <h2 class="flipInX animated wow">Who we are</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of typsse and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</p><br />
                        <a href="#">Know More</a>
                    </div>
                    <div class="col-sm-2 col-xs-hidden"></div>
                </div>
            </div>
        </div>
   </section>-->

<div class="container hidden">
    <div class="row">
        <div class="col-xs-12">
            <div class="home-demo">
                <h2 class="text-center flipInX animated wow">Testimonial</h2>
                <div class="row">
                    <div class="large-12 columns">
                        <div class="owl-carousel">
                            <div class="item fadeInDown animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item fadeInUp animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item fadeInDown animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item fadeInUp animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item fadeInDown animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item fadeInUp animated wow">
                                <div class="testimonials">
                                    <p>“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.”</p>
                                    <ul>
                                        <li>- Glimpsefive <br> xxxxx</li>
                                        <li class="testi-icon"><img class="" src="images/testimonials-quotes.png"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<?php include "footer.php";?>
