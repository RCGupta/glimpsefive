<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Glimpse Five</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Magnific Popup -->
    <!-- <link rel="stylesheet" href="css/magnific-popup.css"> -->
    <link rel="icon" href="images/favicon.png" type="image/png" sizes="64x64">

    <link href="css/hover.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->


</head>

<body>

    <header class="header">
        <div class="header-menu fixed-top">
            <nav class="navbar navbar-default menu--iris">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php" class="navbar-brand"><img class="img-responsive" src="images/logo.svg"></a>
                </div>
                <!-- Collection of nav links and other content for toggling -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav pull-right menu__list">
                        <li class="menu__item <?php if($page=='index'){echo 'active';}?>"><a href="index.php" class="active menu__link">Home</a></li>
                        <li class="menu__item <?php if($page=='about'){echo 'active';}?>"><a href="about.php" class="menu__link">About</a></li>
                        <li class="menu__item <?php if($page=='services'){echo 'active';}?>"><a href="services.php" class="menu__link">Services</a></li>
                        <li class="menu__item <?php if($page=='products'){echo 'active';}?>"><a href="products.php" class="menu__link">Products</a></li>
                    <!--
                        <li class="menu__item <?php if($page=='resources'){echo 'active';}?>"><a href="resources.php" class="menu__link">Resources</a></li>
                        <li class="menu__item <?php if($page=='job-seekers'){echo 'active';}?>"><a href="job-seekers.php" class="menu__link">Upload CV</a></li>
                        <li class="menu__item <?php if($page=='faqs'){echo 'active';}?>"><a href="faqs.php" class="menu__link">FAQs</a></li>
                    -->
                        <li class="menu__item <?php if($page=='contact'){echo 'active';}?>"><a href="contact.php" class="menu__link">Contact</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>