<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="footer">
                <div class="col-xs-12 col-md-4 ">
                    <div class="newsletter">
                        <h4 class="wow animated flipInX">Newsletter</h4>
                        <!--
                                <form class="wow animated fadeInUp">
                                    <input type="text" class="form-control" placeholder="Name">
                                    <input type="text" class="form-control" placeholder="Your Email">
                                    <a href="#" class="btn btn-primary">Submit</a>
                                </form>
                            -->

                        <div class="wow animated fadeInUp">
                            <!--[if lte IE 8]>
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                <![endif]-->
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                            <script>
                                hbspt.forms.create({
                                    portalId: "3004220",
                                    formId: "23915bcc-1163-4054-ae53-a0c3d379ff6a"
                                });

                            </script>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="social-links">
                        <h4 class="wow animated flipInX">Social Links</h4>
                        <ul class="wow animated fadeInUp">
                            <li class="facebook hvr-float-shadow"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="google-plus hvr-float-shadow"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li class="twitter hvr-float-shadow"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li class="linkedin hvr-float-shadow"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="address">
                        <h4 class="wow animated flipInX">Address</h4>
                        <ul class="wow animated fadeInUp">
                            <li class="find-address">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <address>GlimpseFive Solutions Pvt Ltd <br> 204, Amar Neptune, Baner Rd, <br> Baner, Pune - 411045, <br> Maharashtra, India</address>
                            </li>
                            <li class="mail"><a href="mailto:Info@glimpsefive.com"><i class="fa fa-envelope" aria-hidden="true"></i><span>Info@glimpsefive.com</span></a></li>
                            <li class="call"><a href="tel:+919890001912"><i class="fa fa-phone" aria-hidden="true"></i> <span>+91 98900 01912</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="copyrights">
                <div class="col-xs-12 col-md-2">
                    <a href="index.php"><img src="images/logo.svg" width="160"></a>
                </div>
                <div class="col-xs-12 col-md-6 text-right">
                    <p>© 2018 All rights reserved. Designed By <a href="https://www.digiby.com/" target="_blank">Digiby.com</a></p>
                </div>

                <div class="col-xs-12 col-md-4">
                    <ul class="footer-links">
                        <li><a href="#">Terms of use</a></li>
                        <li>|</li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- /.Footer -->

<!-- jQuery -->

<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Owl Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/magnific-popup-options.js"></script>
<!-- wow -->
<script src="js/wow.js"></script>


<!-- Main JS (Do not remove) -->
<script src="js/main.js"></script>


<script>
    $(window).scroll(function() {
        if ($(window).scrollTop() >= 150) {
            $('.header').addClass('fixed-header');
        } else {
            $('.header').removeClass('fixed-header');
        }
    });

</script>

<script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        center: false,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        margin: 10,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });
    
    var owl = $('.owl-carousel-1');
    owl.owlCarousel({
        autoplay: true,
        center: false,
        loop: true,
        nav: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1170: {
                items: 3
            }
        }
    });

    $(document).ready(function() {
        var owl = $('.owl-carousel-logos');
        owl.owlCarousel({
            items: 6,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 1500,
            autoplayHoverPause: true,
            responsive: {
            0: {
                items: 3
            },
            600: {
                items: 4
            },
            1170: {
                items: 6
            }
        }
        });
    });

    new WOW().init();
    

    $(document).ready(function() {
        $(".scroll-to-form").click(function() {
             $('html, body').animate({
                 scrollTop: ($(".products-form-bg").offset().top-50)
             }, 1500);
         });
    });

</script>

</body>

</html>
