<?php 
$page = 'job-seekers';
include "header.php";?>

    <section class="contact-banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="services-bg">
                        <img class="img-responsive jobseekers-img wow fadeInDown animated" src="images/job-seekers-banner.png">
                        <div class="services-banner-content wow fadeInUp  animated">
                            <h1>Job Seekers</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--
    <section class="hiring-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="job-seekers-1">
                        <img class="img-responsive" src="images/job-seekers-img-01.png">
                        <div class="recruiter">
                            <h2 class="wow fadeInDown animated">Connect with our recruiter</h2>
                            <p class="wow fadeInDown animated">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum. <a href="#">Click Here</a></p>
                        </div>
                    </div>
                    <div class="job-seekers-2">
                        <div class="submit-resume">
                            <h2 class="wow fadeInDown animated">Submit Resume</h2>
                            <p class="wow fadeInDown animated">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum. <a href="#">Click Here</a></p>
                        </div>
                        <img class="img-responsive" src="images/job-seekers-img-02.png">
                    </div>
                    <div class="job-seekers-3">
                        <img class="img-responsive" src="images/job-seekers-img-03.png">
                        <div class="search-job">
                            <h2 class="wow fadeInDown animated">Search for a Job</h2>
                            <p class="wow fadeInDown animated">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum. <a href="#">Click Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
-->



<section class="products-form-bg upload-cv-form">
    <div class="container">
        <div class="row">
            <div class="products-page-form">
                <div class="col-xs-12 col-sm-4">
                    <div class="products-form-content">
                        <h4>GlimpseATS provides you more effective onboarding and creates best impression about the company’s culture</h4>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="form-products">
                        <h2 class="text-center">Upload your CV</h2>
                        <p class="text-center">Our recruiters will get back to you to have more details.</p>
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                          hbspt.forms.create({
                            portalId: "3004220",
                            formId: "ad982a2c-20ec-4fbe-bd10-7b9b74b2ac57"
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Serial Number</th>
                        <th>Job Description</th>
                        <th>Years Experience</th>
                        <th>Location</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>01</td>
                        <td>GlimpseATS provides you more effective onboarding and creates best<br>  impression about the company’s culture</td>
                        <td>4 Years</td>
                        <td>Bangalore</td>
                      </tr>
                      <tr>
                        <td>02</td>
                        <td>GlimpseATS provides you more effective onboarding and creates best <br> impression about the company’s culture</td>
                        <td>4 Years</td>
                        <td>Bangalore</td>
                      </tr>
                      <tr>
                        <td>03</td>
                        <td>GlimpseATS provides you more effective onboarding and creates best <br>  impression about the company’s culture</td>
                        <td>4 Years</td>
                        <td>Bangalore</td>
                      </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


    <?php include "footer.php";?>