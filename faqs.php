<?php 
    $page = 'faqs';
    include "header.php";?>


<section class="contact-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="services-bg">
                    <img class="img-responsive jobseekers-img wow fadeInDown animated" src="images/faqs-banner.png">
                    <div class="services-banner-content wow fadeInUp  animated">
                        <h1>FAQs</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="faqs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <p><strong>Do we need to install any software?</strong></p>
                <p>No. It is on the cloud and is supported by almost all the browsers. There is no installations required. Every update is done remotely. All your data and information is stored securely on the cloud that can be accessed from anywhere or any device. In short, the teams can work through it being mobile.</p>
                <p><strong>How do you ensure that my data is secure in the cloud?</strong></p>
                <p>We provide best of the security policies to secure your data. Some of the steps and efforts we take to secure your data is -</p>
                <ul>
                    <li>Replicating your data to our DR framework and performing realtime DR drills on the production data</li>
                    <li>Ensuring the administrating accesses are logged and limited to those who require accesses</li>
                </ul>
                <p><strong>Do we need to purchase the entire site or we can even buy the few modules to meet the business requirements?</strong></p>
                <p>Yes, we can make a custom made application to suite your business requirements.  You can also purchase a specific module and we can integrate with your existing softwares</p>
                <p><strong>Does your software cater to the multi-national companies, which has offices at many locations separated by different languages?</strong></p>
                <p>Yes, it is very flexible and can be enabled for various languages on the request</p>
                <p><strong>How long does it take to make it up and running?</strong></p>
                <p>For medium to large organisation it takes 4 to 6 weeks. For smaller organisation it can be accomplished within 2 weeks</p>
                <p><strong>Do you also work with NGOs, educational institutes and smaller companies?</strong></p>
                <p>Yes and we customise the special commercial packages for them</p>
            </div>
        </div>
    </div>
</section>








<?php include "footer.php";?>