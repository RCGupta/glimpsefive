<?php 
$page = 'contact';
include "header.php";?>

<section class="contact-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="services-bg contact-bnr">
                    <img class="img-responsive wow fadeInDown animated" src="images/contact-banner-mobile.png">
                    <div class="services-banner-content  wow fadeInUp  animated contact-heading">
                        <h1>Contact</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="contact-banner hidden">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="contact-bg">
                    <img class="img-responsive hidden-sm visible-md visible-lg" src="images/contact-banner.png">
                    <img class="img-responsive visible-sm hidden-md mobile-banner" src="images/contact-banner-mobile.png">
                    <div class="banner-content wow fadeInUp  animated">
                        <h1>Contact</h1>
                        <!--    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-form">
    <div class="container">
        <div class="row">
            <div class="form-bg">
                <div class="col-xs-12 col-sm-4">
                    <div class="left-side-address">
                        <h1>Find Us</h1>
                        <p class="contact-address"><strong>GlimpseFive Solutions Pvt Ltd</strong> <br> 204, Amar Neptune, Baner Rd, <br> Baner, Pune - 411045, <br> Maharashtra, India</p>
                        <p class="contact-address"><strong>GlimpseFive LLC,</strong> <br> 16192 Coastal Hwy Lewes,<br> Delaware 19958, <br> United States Of America</p>
                        <h1>Mail Us</h1>
                        <a href="mailto:Info@glimpsefive.com">Info@glimpsefive.com</a>
                        <h1>Call Us</h1>
                        <a href="#">+91 98900 01912</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="right-side-form">
                        <!--
                                <form action="/action_page.php">
                                    <div class="form-group col-xs-6">
                                        <input type="text" class="form-control" id="firstname" placeholder="First Name" name="First Name" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="text" class="form-control" id="lastname" placeholder="Last Name" name="Last Name" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="email" class="form-control" id="email" placeholder="Email" name="Email" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="number" class="form-control" id="phonenumber" placeholder="Phone Number" name="Phone Number" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="text" class="form-control" id="companyname" placeholder="Company Name" name="Company Name" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="text" class="form-control" id="country" placeholder="Country" name="Country" required>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <textarea class="form-control" rows="5" id="comment" placeholder="How can we help you?"></textarea>
                                    </div>
                                    <div class="col-xs-12"> <button type="submit" class="contact-submit">Submit</button></div>

                                </form>
                            -->

                        <!--[if lte IE 8]>
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                            <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "3004220",
                                formId: "f7f77e0e-ab6c-47df-880b-d2c176b75fa2"
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "footer.php";?>