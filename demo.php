<?php 
$page = 'job-seekers';
include "header.php";?>

    


<section class="products-form-bg demo">
    <div class="container">
        <div class="row">
            <div class="products-page-form">
                <div class="col-xs-12 col-sm-4">
                    <div class="products-form-content">
                        <h4>GlimpseATS provides you more effective onboarding and creates best impression about the company’s culture</h4>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="form-products">
                        <h2 class="text-center text-uppercase">Demo</h2>
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                          hbspt.forms.create({
                            portalId: "3004220",
                            formId: "1a960c16-eb65-4f5a-9fbf-86f5a80a46d0"
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



    <?php include "footer.php";?>