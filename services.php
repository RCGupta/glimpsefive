<?php 
$page = 'services';
include "header.php";?>

<section class="contact-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="services-bg">
                    <img class="img-responsive wow fadeInDown animated" src="images/service-banner.png">
                    <div class="services-banner-content  wow fadeInUp  animated">
                        <h1>Services</h1>
                        <h4>Connecting You to the best talent pool</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="solutions">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-center wow animated flipInX">Solutions</h2>
            </div>
            <!-- <div class="col-xs-2 hidden-xs"></div> -->
            <div class="col-xs-12 text-center">
                <div class="col-xs-12 col-sm-6 col-lg-3 fadeInUp animated wow">
                    <div class="candidate">
                        <!--  <ul>
                                <li><img width="50" src="images/icon-1.svg"></li>
                                <li>Candidate Pipeline</li>
                            </ul> -->
                        <a href="services.php">
                            <img class="img-responsive candidate-img" src="images/candidate-pipeline.png" />
                            <p class="candidate-title">Candidate Pipeline</p>
                            <p> Source the validated candidates from your own repository. </p>
                        </a>
                        <!-- <a href="#">know More</a> -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3 fadeInDown animated wow">
                    <div class="candidate">
                        <!-- <ul>
                                <li><img width="50" src="images/icon-2.svg"></li>
                                <li>E-mail Marketing</li>
                            </ul> -->
                        <a href="services.php">
                            <img class="img-responsive candidate-img" src="images/email-marketing.png" />
                            <p class="candidate-title">E-mail Marketing</p>
                            <p> Get the right messages to the right candidates about the company and role. </p>
                        </a>
                        <!-- <a href="#">know More</a> -->
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-3 fadeInUp animated wow">
                    <div class="candidate">
                        <!-- <ul>
                                <li><img width="50" src="images/icon-3.svg"></li>
                                <li>Campaigning</li>
                            </ul> -->
                        <a href="services.php">
                            <img class="img-responsive candidate-img" src="images/campaigning.png" />
                            <p class="candidate-title">Campaigning</p>
                            <p>Dedicated campaigns for the job openings, preferably from our own wide networks.</p>
                        </a>
                        <!-- <a href="#">know More</a> -->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3 fadeInDown animated wow">
                    <div class="candidate">
                        <!-- <ul>
                                <li><img width="50" src="images/icon-4.svg"></li>
                                <li>Reporting</li>
                            </ul> -->
                        <a href="services.php">
                            <img class="img-responsive candidate-img" src="images/reporting.png" />
                            <p class="candidate-title">Reporting</p>
                            <p>comprehensive detailed online report of the selection ratios.</p>
                        </a>
                        <!-- <a href="#">know More</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hiring-services">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="services-hiring-image">
                    <img class="img-responsive d-inline-block wow fadeInUp animated" src="images/hiring-servics.png">
                </div>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="services-hiring-content wow fadeInDown animated">
                    <h1>More About Hiring Services.</h1>
                    <h4>Candidate Pipeline - Source the validated candidates from your own repository</h4>
                    <p>We built the pipeline for your regular and niche skills. Offer the pre-screened candidates immediately into your ATS or according to the choice of communication</p>
                    <p>The pipeline automatically picks and recommends the candidates from the repository of required skills and behaviour. There could be the candidates who have shown interest in your company but haven’t applied for the job.</p>
                    <p>You can see the progress of the recruitment at real time if you opt for our candidate onboarding module</p>

                    <h4>E-mail Marketing - Get the right messages to the right candidates about the company and role</h4>
                    <p>The personalised messaging to every candidate, so that to identify only the interested candidates to reduce blackouts, to ensure genuinely interested flow of candidates. Our robust referral candidate system get us the passive candidates who are interested for the serious career options.</p>

                    <h4>Campaigning - Dedicated campaigns for the job openings, preferably from our own wide networks.</h4>
                    <p> We pre-screen the candidates before they are put into the recruitment cycle.</p>

                    <h4>Reporting - comprehensive detailed online report of the selection ratios.</h4>
                    <p> for any job openings, we give the detailed report on how many folks did we speak or connected, out of which how many of them shown interest in your organisation, how many appeared for the interview and finally how many of them got selected. You can leverage on our on-boarding module, however, we provide this information as this we feel can’t be ignored as it provides good insights to the marketing team.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="products-form-bg services-form">
    <div class="container">
        <div class="row">
            <div class="products-page-form">
                <div class="col-xs-12 col-sm-4">
                    <div class="products-form-content">
                        <h4>GlimpseATS provides you more effective onboarding and creates best impression about the company’s culture</h4>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <div class="form-products">
                        <h2 class="text-center  text-uppercase">Talk to us</h2>
                        <!--[if lte IE 8]>
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "3004220",
                                formId: "7f1479f6-412d-4fa4-a5e0-e5fe2cf60769"
                            });

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "footer.php";?>
