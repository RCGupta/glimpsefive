<?php 
$page = 'about';
include "header.php";?>

<section class="contact-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="services-bg about-banner">
                    <img class="img-responsive wow fadeInDown animated" src="images/about-us.png">
                    <div class="services-banner-content  wow fadeInUp  animated">
                        <h1>About</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <p>We are a people’s company, differentiated by technology !</p>
                <p>At GlimpseFive, we believe there is a better way to do staffing. Our mission is to render the value in being obsessively passionate but less invasive about finding the right fit for people and organisations with incredible experience and transparency. We’re excited to simplify hiring through our approach, processes and software.</p>
                <p><strong>How the name Glimpsefive emerged ?</strong></p>
                <p>Inspired by few great people and few great organisations who have scaled up on their own. Our inspirations are drawn from their common beliefs as integrity, friendly, passion, authenticity, approach.</p>
            </div>
        </div>
    </div>
</section>





<?php include "footer.php";?>
