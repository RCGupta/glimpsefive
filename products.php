<?php 
$page = 'products';
include "header.php";?>

<section class="contact-banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="services-bg">
                    <img class="img-responsive wow fadeInDown animated" src="images/products-banner.png">
                    <div class="services-banner-content products-content wow fadeInUp  animated">
                        <h1>GlimpseATS</h1>
                        <p>GlimpseATS automates operational tasks and frees HR time for the high value tickets to make difference into people for your organisation</p>
                        <a href="#" class="scroll-to-form">Calling for the Demo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--
<section class="thumbnail-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 text-center">
                <img class="img-responsive" src="images/very-simple-to-operate.png">
                <h3>Very simple to operate</h3>
            </div>
            <div class="col-xs-12 col-sm-4 text-center">
                <img class="img-responsive" src="images/hiring-activities.png">
                <h3>All information right from hiring activities till retire at one place</h3>
            </div>
            <div class="col-xs-12 col-sm-4 text-center">
                <img class="img-responsive" src="images/flexible-analytics.png">
                <h3>Flexible Analytics</h3>
            </div>
        </div>
    </div>
</section>
-->


<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="home-demo">
                <div class="large-12 columns">
                    <div class="owl-carousel-1">
                        <div class="col-xs-12">
                            <div class="item">
                                <div class="products-slider">
                                    <div class="products-image">
                                        <img class="img-responsive" src="images/candidate-experience.png">
                                        <h3 class="slider-heading">Candidate Experience</h3>
                                    </div>
                                    <p>GlimpseATS creates a positive experience from start to finish, with an easy-to-follow process on the candidate side and the communication on the hiring side. As a result, candidates feel acknowledged and informed. Your reputation as an employee friendly attitude is always appreciated. Software helps you automate onboard of the incoming employee, making it easier to engage before their first day and set them up for happy employment.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <div class="item">
                                <div class="products-slider">
                                    <div class="products-image">
                                        <img class="img-responsive" src="images/seamless-communication.png">
                                        <h3 class="slider-heading">Seamless Communication</h3>
                                    </div>
                                    <p>58 percent of applicants who have a bad hiring experience cite poor communication as a main reason. GlimpseFive ATS makes communication fast and efficient with customizable email templates that allow you to send messages to multiple candidates simultaneously.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <div class="item">
                                <div class="products-slider">
                                    <div class="products-image">
                                        <img class="img-responsive" src="images/easy-to-use.png">
                                        <h3 class="slider-heading">Easy to Use</h3>
                                    </div>
                                    <p>GlimpseATS makes it easy to post job openings and create hiring teams to collaborate on candidates. Collaborators can comment and rate candidates, while HR team controls the visibility of sensitive information like desired salary.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <div class="item">
                                <div class="products-slider">
                                    <div class="products-image">
                                        <img class="img-responsive" src="images/create-personal-connections.png">
                                        <h3 class="slider-heading">Create Personal Connections</h3>
                                    </div>
                                    <p>Make informal introductions as a part of the onboarding process with the “Get to Know You Page”. Simply pick the informal questions from the repository to ask the incoming employee as their favorite vacation location, restaurants, you yearn to get etc, and direct them to the employees who should receive their responses. This helps team members create personal connections with new hires right from the day-one. New hires also get to know the teammates so on the first day they will feel at home.</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            <div class="item">
                                <div class="products-slider">
                                    <div class="products-image">
                                        <img class="img-responsive" src="images/digital-onboarding.png">
                                        <h3 class="slider-heading">Digital Onboarding</h3>
                                    </div>
                                    <p>The new hire packet automates the onboarding process by sending important forms to incoming employees before their first day. You can also communicate first-day instructions so they know exactly where to go and what to do. <br>
                                    Not only does this saves your time of answering repeatable questions, it also helps the new hire have an incredible first day.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="read-more hidden">
    <div class="container">
        <div class="row">
            <div class="candidate-section">
                <div class="col-xs-4">
                    <img class="img-responsive" src="images/candidate-experience.png">
                </div>
                <div class="col-xs-8">
                    <h3>Candidate Experience</h3>
                    <p>GlimpseATS creates a positive experience from start to finish, with an easy-to-follow process on the candidate side and the communication on the hiring side. As a result, candidates feel acknowledged and informed. Your reputation as an employee friendly attitude is always appreciated. Software helps you automate onboard of the incoming employee, making it easier to engage before their first day and set them up for happy employment.</p>
                </div>
            </div>

            <div class="Seamless-section">
                <div class="col-xs-8">
                    <h3>Seamless Communication</h3>
                    <p>58 percent of applicants who have a bad hiring experience cite poor communication as a main reason. GlimpseFive ATS makes communication fast and efficient with customizable email templates that allow you to send messages to multiple candidates simultaneously.</p>
                </div>
                <div class="col-xs-4">
                    <img class="img-responsive" src="images/candidate-experience.png">
                </div>
            </div>

            <div class="candidate-section">
                <div class="col-xs-4">
                    <img class="img-responsive" src="images/candidate-experience.png">
                </div>
                <div class="col-xs-8">
                    <h3>Easy to Use</h3>
                    <p>GlimpseATS makes it easy to post job openings and create hiring teams to collaborate on candidates. Collaborators can comment and rate candidates, while HR team controls the visibility of sensitive information like desired salary.</p>
                </div>
            </div>

            <div class="Seamless-section">
                <div class="col-xs-4">
                    <img class="img-responsive" src="images/candidate-experience.png">
                </div>
                <div class="col-xs-8">
                    <h3>Create Personal Connections</h3>
                    <p>Make informal introductions as a part of the onboarding process with the “Get to Know You Page”. Simply pick the informal questions from the repository to ask the incoming employee as their favorite vacation location, restaurants, you yearn to get etc, and direct them to the employees who should receive their responses. This helps team members create personal connections with new hires right from the day-one. New hires also get to know the teammates so on the first day they will feel at home</p>
                </div>
            </div>

            <div class="candidate-section">
                <div class="col-xs-4">
                    <img class="img-responsive" src="images/candidate-experience.png">
                </div>
                <div class="col-xs-8">
                    <h3>Digital Onboarding</h3>
                    <p>The new hire packet automates the onboarding process by sending important forms to incoming employees before their first day. You can also communicate first-day instructions so they know exactly where to go and what to do.</p>
                    <p>Not only does this saves your time of answering repeatable questions, it also helps the new hire have an incredible first day.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hiring-services hidden">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="products">
                    <!--
                    <img class="img-responsive pull-left wow fadeInDown animated" src="images/products-page-image.png">
                    <div class="products-content text-justify wow fadeInUp animated">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
-->

                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-and-products">
    <div class="container">
        <div class="row">
            <h2 class="text-center flipInX animated wow text-uppercase">Learn About Our Product</h2>
            <div class="col-xs-1 hidden-xs hidden-sm" style="margin-left:5%;"></div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="cloud">
                    <img class="img-responsive" src="images/img_01.png">
                    <div class="cloud-content">
                        <h4>Cloud Based HR</h4>
                        <p>Cloud-based means that you don't need your own server infrastructure. This makes you free from the hassles of maintaining the servers and data backups. It is easily scalable - depends on company size, you can contact us anytime for assistance. Just sign up and make your HR easier</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="cloud">
                    <img class="img-responsive" src="images/img_02.png">
                    <div class="cloud-content">
                        <h4>Secure And Available with History</h4>
                        <p>All employee data including, right from the first automated but customised invitation till the onboard, every information and documentation is captured and kept in the secured place. We use the world class hosting provider - Google Web Services. All connections are encrypted, your data is backed up and systems are monitored 24/7 by our IT experts</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="cloud">
                    <img class="img-responsive" src="images/img_03.png">
                    <div class="cloud-content">
                        <h4>Multi-Location / Country</h4>
                        <p>Employees hiring in different countries? Talent Onboard Software handles multiple countries configuration in one account. We help you in enabling and configuring according to the required processes followed in different countries.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-3 hidden-xs hidden-sm"></div>
            <div class="col-xs-3 hidden-xs hidden-sm"></div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="cloud">
                    <img class="img-responsive" src="images/img_04.png">
                    <div class="cloud-content">
                        <h4>Integration</h4>
                        <p>Seamless integrations with tools that are commonly used by growing businesses. You can enable integration with just one click and keep your HR data synchronized automatically with your existing systems.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="cloud">
                    <img class="img-responsive" src="images/img_05.png">
                    <div class="cloud-content">
                        <h4>Candidate Management - Simple Operations</h4>
                        <p>Talent Onboard Software is a web application to access it from everywhere in your web browser. Your essential HR activities are always at hand on any of the handheld device for convenience.</p>
                    </div>
                </div>
            </div>
        </div>
<!--
        <div class="row" style="margin-top:30px;">
            
        </div>
-->
    </div>
</section>

<section class="faqs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><strong>Your Questions - Answered</strong></h1>
                <p><strong>Do we need to install any software?</strong></p>
                <p>No. It is on the cloud and is supported by almost all the browsers. There is no installations required. Every update is done remotely. All your data and information is stored securely on the cloud that can be accessed from anywhere or any device. In short, the teams can work through it being mobile.</p>
                <p><strong>How do you ensure that my data is secure in the cloud?</strong></p>
                <p>We provide best of the security policies to secure your data. Some of the steps and efforts we take to secure your data is -</p>
                <ul>
                    <li>Replicating your data to our DR framework and performing realtime DR drills on the production data</li>
                    <li>Ensuring the administrating accesses are logged and limited to those who require accesses</li>
                </ul>
                <p><strong>Do we need to purchase the entire site or we can even buy the few modules to meet the business requirements?</strong></p>
                <p>Yes, we can make a custom made application to suite your business requirements. You can also purchase a specific module and we can integrate with your existing softwares</p>
                <p><strong>Does your software cater to the multi-national companies, which has offices at many locations separated by different languages?</strong></p>
                <p>Yes, it is very flexible and can be enabled for various languages on the request</p>
                <p><strong>How long does it take to make it up and running?</strong></p>
                <p>For medium to large organisation it takes 4 to 6 weeks. For smaller organisation it can be accomplished within 2 weeks</p>
                <p><strong>Do you also work with NGOs, educational institutes and smaller companies?</strong></p>
                <p>Yes and we customise the special commercial packages for them</p>
            </div>
        </div>
    </div>
</section>

<section class="products-form-bg">
    <div class="container">
        <div class="row">
            <div class="products-page-form">
                <div class="col-xs-12 col-sm-4">
                    <div class="products-form-content">
                        <h4>GlimpseATS provides you more effective onboarding and creates best impression about the company’s culture</h4>
                        <ul>
                            <li>Very simple to operate</li>
                            <li>All information right from hiring activities till retire at one place</li>
                            <li>Flexible analytics</li>
                            <!-- <i class="fa fa-angle-right" aria-hidden="true"></i> -->
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8">   
                    <div class="form-products">
                        <h2 class="text-center text-uppercase">Talk to us</h2>
                        <!--[if lte IE 8]>
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "3004220",
                                formId: "bcb96dcb-327e-4b3d-82c4-c1da2a97c963"
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include "footer.php";?>